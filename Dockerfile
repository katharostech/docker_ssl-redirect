################
# Redirect http traffic to https (80 to 443)
################

# Set the base image
FROM katharostech/nginx

# File Author / Maintainer
MAINTAINER orion-pax dkhaws77@gmail.com

# Replace the image's existing nginx configuration
COPY my-default.conf /etc/nginx/conf.d/my-default.conf
RUN chmod 644 /etc/nginx/conf.d/my-default.conf

# Don't need a command as that is handled in the base image.
# CMD ["nginx"]
