# What is this image for?
The purpose of this image is simple.  Accept http (80) traffic and redirect it to https (443).  This is accomplished using a simple implementation of nginx.

# Running the container
```bash
docker run -h ssl-redirect \
--name ssl-redirect \
-p 80:80 \
-dt katharostech/ssl-redirect
```
